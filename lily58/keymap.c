#include QMK_KEYBOARD_H

#define CTL_ESC LCTL_T(KC_ESC)
#define SCRNSHT S(KC_PSCR)

enum layer_number {
  _BASE,
  _NUM,
  _SYM,
  _NAV,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

 [_BASE] = LAYOUT( \
  KC_ESC,   KC_1,   KC_2,    KC_3,    KC_4,    KC_5,                   KC_6,    KC_7,    KC_8,    KC_9,    KC_0,     KC_DEL, \
  KC_TAB,   KC_Q,   KC_W,    KC_E,    KC_R,    KC_T,                   KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,     KC_BSPC, \
  CTL_ESC,  KC_A,   KC_S,    KC_D,    KC_F,    KC_G,                   KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN,  KC_QUOT, \
  KC_LSFT,  KC_Z,   KC_X,    KC_C,    KC_V,    KC_B, KC_PGUP,  KC_PGDN,  KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH,  KC_ENT, \
                        KC_LALT, KC_LGUI, MO(_NUM), KC_LSFT,   KC_SPC, MO(_SYM), KC_RALT, KC_RGUI \
),

[_NUM] = LAYOUT( \
  KC_NO,    KC_NO,  KC_NO,   KC_NO,  KC_NO,  KC_NO,                     KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,    KC_NO, \
  _______,  KC_F9,  KC_F10,  KC_F11, KC_F12, KC_NO,                     KC_NO,  KC_7,   KC_8,   KC_9,   KC_0,     _______, \
  _______,  KC_F5,  KC_F6,   KC_F7,  KC_F8,  KC_NO,                     KC_NO,  KC_4,   KC_5,   KC_6,   KC_PPLS,  KC_PAST, \
  _______,  KC_F1,  KC_F2,   KC_F3,  KC_F4,  KC_NO, KC_HOME,   KC_END, KC_NO,  KC_1,   KC_2,   KC_3,   KC_PMNS, KC_PSLS, \
                    KC_NO,   KC_NO,  _______, KC_NO,                    KC_SPC,  KC_0,   KC_DOT, KC_NO \
),

[_SYM] = LAYOUT( \
  _______, _______, _______, _______, _______, _______,                _______, _______, _______, _______, _______, _______, \
  _______, KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC,                KC_CIRC,  KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, KC_BSPC, \
  _______, KC_INS,  KC_HOME, KC_PGUP, KC_PSCR, KC_NO,                  KC_TILD,  KC_MINS, KC_PLUS, KC_LBRC, KC_RBRC, KC_PIPE, \
  _______, KC_PAUS,  KC_END, KC_PGDN, SCRNSHT, KC_NO, KC_NO,     KC_NO, KC_GRV,  KC_UNDS, KC_EQL,  KC_LCBR, KC_RCBR, KC_BSLS, \
                    KC_NO,   _______, MO(_NAV), _______,               KC_NO, _______, KC_NO, KC_NO \
),

  [_NAV] = LAYOUT( \
  _______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, _______, \
  _______, XXXXXXX, XXXXXXX, KC_UP, XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, _______, \
  _______, XXXXXXX, KC_LEFT, KC_DOWN, KC_RIGHT, XXXXXXX,                   KC_LEFT, KC_DOWN, KC_UP, KC_RIGHT, XXXXXXX, XXXXXXX, \
  _______, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, _______,\
                             _______, _______, _______, _______, _______,  _______, _______, _______ \
  )
};
