/*
Copyright 2019 @foostan
Copyright 2020 Drashna Jaelre <@drashna>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#pragma once


/* Select hand configuration */

// #define MASTER_LEFT
//#define MASTER_RIGHT
#define EE_HANDS

#define TAPPING_TERM 100
#define TAPPING_FORCE_HOLD

#define MOUSEKEY_DELAY 0
#define MOUSEKEY_INTERVAL 28

#ifdef RGBLIGHT_ENABLE
    #undef RGBLED_NUM
    #undef RGBLED_SPLIT
    #define RGBLIGHT_EFFECT_TWINKLE
    #define RGBLED_NUM 54
    #define RGBLED_SPLIT \
     { 27, 27 }
    #define RGBLIGHT_STARTUP_VAL 100
    #define RGBLIGHT_VAL_STEP 25
#endif

#define LAYER_STATE_8BIT
#define OLED_FONT_H "keyboards/crkbd/lib/glcdfont.c"

#define PIMORONI_TRACKBALL_ROTATE
#define PIMORONI_TRACKBALL_INVERT_X
