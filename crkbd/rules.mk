MOUSEKEY_ENABLE     = yes    # Mouse keys
EXTRAKEY_ENABLE     = yes
OLED_DRIVER_ENABLE  = no
RGBLIGHT_ENABLE     = yes
RGB_MATRIX_ENABLE   = no
LTO_ENABLE          = yes
SPLIT_KEYBOARD      = yes
PIMORONI_TRACKBALL_ENABLE = no


ifeq ($(strip $(PIMORONI_TRACKBALL_ENABLE)), yes)
 POINTING_DEVICE_ENABLE = yes
 SRC += drivers/sensors/pimoroni_trackball.c
 QUANTUM_LIB_SRC += i2c_master.c
endif
